﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XUnitTestRestApi.Models
{
    public class UserTest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public UserTest(string Email, string Password, string FirstName, string LastName)
        {
            this.Email = Email;
            this.Password = Password;
            this.FirstName = FirstName;
            this.LastName = LastName;
        }
    }

    public class TokenModel
    {
        public string token { get; set; }
    }

}
