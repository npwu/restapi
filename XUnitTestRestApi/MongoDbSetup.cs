﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestApi;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using MongoDB.Driver;
using RestApi.Models;


namespace XUnitTestRestApi
{
    public class MongoDbSetup : IDisposable
    {
        public HttpClient Client { get; private set; }
        private static readonly string filePath = Path.GetFullPath("../../../../RestApi/appsettings.json");

        // define const used by tests
        public static readonly string TestUser = JsonConvert.SerializeObject(new { Email = "test@test", Password = "mysecurepassword", FirstName = "Test", LastName = "LastName" });
        public static readonly string AuthEndpoint = "/api/auth";

        public MongoDbSetup()
        {
            var configuration = new ConfigurationBuilder().AddJsonFile(filePath).Build();

            TestServer server = new TestServer(new WebHostBuilder()
                .UseConfiguration(configuration)
                .UseEnvironment("Test")
                .UseStartup<Startup>());

            Client = server.CreateClient();

        }

        public void Dispose()
        {
            var configuration = new ConfigurationBuilder().AddJsonFile(filePath).Build();

            MongoDbSettings db = new MongoDbSettings();

            db.mongoClient = new MongoClient(configuration.GetConnectionString("MongoUrl"));
            db.mongoDatabase = db.mongoClient.GetDatabase(configuration.GetSection("MongoDbDatabase:TestDatabase").Value);

            db.mongoDatabase.DropCollection("Users");
        }
    }
}
