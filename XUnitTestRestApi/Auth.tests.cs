﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestApi;
using Xunit;
using FluentAssertions;
using System.Net;
using RestApi.Services;
using RestApi.Controllers;
using Xunit.Abstractions;
using Newtonsoft.Json.Linq;
using XUnitTestRestApi.Models;
namespace XUnitTestRestApi
{
    public class Auth : IClassFixture<MongoDbSetup>
    {

        MongoDbSetup MongoDbSetup;
        private HttpClient Client;
        private string Token;
        public Auth(MongoDbSetup MongoDbSetup)
        {
            this.MongoDbSetup = MongoDbSetup;
            Client = this.MongoDbSetup.Client;
        }

        [Fact]
        public async Task Register()
        {
            TokenModel ExpectedObject = new TokenModel();

            var response = await Client.PostAsync($"{MongoDbSetup.AuthEndpoint}/register",
                new StringContent(MongoDbSetup.TestUser, Encoding.UTF8, "application/json"));


            // check status code
            response.StatusCode.Should().Be(HttpStatusCode.Created);

            //check content
            string responseContent = await response.Content.ReadAsStringAsync();
            var tokenModel = JsonConvert.DeserializeObject<TokenModel>(responseContent);
            this.Token = tokenModel.token;
        }

        [Theory]
        [InlineData(null, "mysecurepassword", "Test", "LastName")]
        [InlineData("test@test", "", "Test", "LastName")]
        [InlineData("test@test", "mysecurepassword", null, "LastName")]
        [InlineData("test@test", "mysecurepassword", "Test", "")]
        public async Task RegisterWithCorruptedData(string Email, string Password, string FirstName, string LastName)
        {
            UserTest userTest = new UserTest(Email, Password, FirstName, LastName);
            var response = await Client.PostAsync($"{MongoDbSetup.AuthEndpoint}/register",
                new StringContent(JsonConvert.SerializeObject(userTest), Encoding.UTF8, "application/json"));

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
    }
}
