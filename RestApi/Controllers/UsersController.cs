﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestApi.Services;
using RestApi.Models;
using Microsoft.AspNetCore.Authorization;
using RestApi.Validator;
using RestApi.Utils;

namespace RestApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly UsersService _usersService;

        public UsersController(UsersService usersService)
        {
            _usersService = usersService;
        }

        /// <summary>
        /// List users
        /// </summary>
        /// <param name="adminListUsers"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<UsersModel>), 200)]
        public ActionResult<List<UsersModel>> Get([FromQuery]ListUsersValidator adminListUsers)
        {
            //NotificationService notificationService = new NotificationService();
            //notificationService.send("This is my notification");
            return Ok(_usersService.List(adminListUsers.PerPage, adminListUsers.Page));
        }

        /// <summary>
        /// Get user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:length(24)}")]
        [ProducesResponseType(typeof(UsersModel), 200)]
        public ActionResult<UsersModel> Get(string id)
        {
            var user = _usersService.Get(id);

            if (user == null)
            {
                throw new CustomNotFound("User not found");
            }

            return Ok(user);
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <remarks>
        ///     
        ///     POST /users
        ///     {
        ///         "FirstName": "New First Name"
        ///     }
        ///     
        /// </remarks>
        /// <param name="updateValidator"></param>
        /// <returns></returns>
        [HttpPatch]
        [ProducesResponseType(typeof(UsersModel), 200)]
        public ActionResult update(UpdateValidator updateValidator)
        {
            if (updateValidator.Email != null && _usersService.EmailExist(updateValidator.Email))
            {
                throw new CustomBadRequest("This email is already linked to another account");
            }

            UsersModel user = _usersService.getUser(HttpContext.User);
            UsersModel newUser = (UsersModel)updateValidator;
            newUser.MergeWith<UsersModel>(user);

            _usersService.Update(user.Id, newUser);

            return Ok(newUser);
        }

        /// <summary>
        /// Return logged user profil
        /// </summary>
        /// <returns></returns>
        [HttpGet("me")]
        [ProducesResponseType(typeof(UsersModel), 200)]
        public ActionResult<UsersModel> GetProfil()
        {
            UsersModel user = _usersService.getUser(HttpContext.User);
            return Ok(user);
        }
    }
}
