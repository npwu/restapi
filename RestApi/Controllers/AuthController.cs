﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestApi.Models;
using RestApi.Services;
using System.Security.Cryptography;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Net;
using System.Net.Http;
using RestApi.Validator;
using Microsoft.AspNetCore.Authorization;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace RestApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AuthController : ControllerBase
    {
        private readonly UsersService _usersService;
        private IConfiguration _config;

        public AuthController(UsersService usersService, IConfiguration config)
        {
            _usersService = usersService;
            _config = config;
        }

        /// <summary>
        /// Register an user
        /// </summary>
        /// <remarks>
        /// Sample request
        /// 
        ///     POST /auth/register
        ///     {
        ///         FirstName: "firstname",
        ///         LastName: "lastname",
        ///         Email: "exemple@gmail.com",
        ///         Password: "mysecurepassword"
        ///     }
        ///     
        /// </remarks>
        /// <param name="user">UsersModel</param>
        [HttpPost("register"), AllowAnonymous]
        [ProducesResponseType(typeof(TokenModel), 201)]
        [ProducesResponseType(typeof(ErrorModel), 400)]
        public ActionResult Register([FromBody]RegisterValidator user)
        {
            var userModel = (UsersModel)user;
            userModel.Role = "User";
            var res = _usersService.Create(userModel);
            if (res == null)
            {
                throw new CustomBadRequest("This email is already linked to another account");
            }
            var tokenString = BuildToken(res);

            return Created("", new TokenModel { token = tokenString });
        }

        /// <summary>
        /// Login an user
        /// </summary>
        /// <remarks>
        /// Sample request
        /// 
        ///     POST /auth/login
        ///     {
        ///         Email: "exemple@gmail.com",
        ///         Password: "mysecurepassword"
        ///     }
        ///     
        /// </remarks>
        /// <param name="login">LoginValidator</param>
        /// <returns></returns>
        [HttpPost("login"), AllowAnonymous]
        [ProducesResponseType(typeof(TokenModel), 201)]
        [ProducesResponseType(typeof(ErrorModel), 401)]
        public ActionResult Login([FromBody]LoginValidator login)
        {
            var user = Authenticate(login);

            if (user == null)
            {
                throw new CustomUnauthorized("Invalid Email or password");
            }

            var tokenString = BuildToken(user);
            return Ok(new TokenModel { token = tokenString });
        }

        /// <summary>
        /// Logout
        /// </summary>
        /// <returns></returns>
        [HttpPost("logout")]
        [ProducesResponseType(204)]
        public ActionResult<UsersModel> Logout()
        {
            return NoContent();
        }

        /// <summary>
        /// Delete user account
        /// </summary>
        /// <returns></returns>
        [HttpDelete("delete")]
        [ProducesResponseType(204)]
        public ActionResult<String> Delete()
        {
            UsersModel user = _usersService.getUser(HttpContext.User);

            _usersService.Remove(user.Id);
            return NoContent();
        }

        private string BuildToken(UsersModel user)
        {
            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.NameId, user.Id),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, user.Role)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddMinutes(30),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private UsersModel Authenticate(LoginValidator login)
        {
            UsersModel user;

            user = _usersService.Get(login.Email, login.Password);

            return user;
        }

    }
}