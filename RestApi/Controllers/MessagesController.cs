﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestApi.Services;
using RestApi.Validator;
using RestApi.Models;
using Microsoft.AspNetCore.SignalR;

namespace RestApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MessagesController : ControllerBase
    {
        private readonly UsersService _usersService;
        private readonly MessageService _messagesService;


        public MessagesController(UsersService usersService, MessageService messageService)
        {
            _messagesService = messageService;
            _usersService = usersService;
        }

        /// <summary>
        /// Post new message
        /// </summary>
        /// <param name="messageValidator"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PostMessage(MessageValidator messageValidator)
        {
            UsersModel TargetUser = _usersService.Get(messageValidator.ToUser);
            if (TargetUser == null)
            {
                throw new CustomBadRequest("Target user does not exist");
            }
            UsersModel user = _usersService.getUser(HttpContext.User);

            MessageModel message = new MessageModel();
            message.IsRead = false;
            message.ToUser = TargetUser.Id;
            message.FromUser = user.Id;
            message.Content = messageValidator.Content;
            message.Date = DateTime.Now;

            _messagesService.AddMessage(message);

            return Ok(message);
        }

        /// <summary>
        /// Get news message
        /// </summary>
        /// <returns></returns>
        [HttpGet("news")]
        public List<UsersModel> GetNewMessages()
        {
            UsersModel user = _usersService.getUser(HttpContext.User);
            List<MessageModel> messageModels = _messagesService.GetNewMessages(user.Id);
            List<UsersModel> users = new List<UsersModel>();
            foreach(MessageModel message in messageModels)
            {
                if (message.ToUser == user.Id && !users.Any(bson => bson.Id == message.FromUser))
                {
                    UsersModel tmp = _usersService.Get(message.FromUser);
                    users.Add(tmp);
                }
            }

            return users;
        }

        /// <summary>
        /// Get message from id
        /// </summary>
        /// <param name="otherUser"></param>
        /// <returns></returns>
        [HttpGet]
        public List<MessageModel> GetMessages([FromQuery] string otherUser)
        {
            UsersModel user = _usersService.getUser(HttpContext.User);
            return _messagesService.getChat(user.Id, otherUser);
        }
    }
}