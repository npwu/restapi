﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RestApi.Services;
using RestApi.Validator;
using RestApi.Models;
using RestApi.Utils;
namespace RestApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NewsController : ControllerBase
    {
        private readonly UsersService _usersService;
        private readonly NewsService _newsService;

        public NewsController(UsersService usersService, NewsService newsService)
        {
            _newsService = newsService;
            _usersService = usersService;
        }

        /// <summary>
        /// Create a News
        /// </summary>
        /// <remarks>
        /// Sample request
        /// 
        ///     POST /news
        ///     {
        ///         Title: "My awsome title",
        ///         Content: "Awsome content"
        ///     }
        ///     
        /// </remarks>
        /// <param name="newsValidator"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ProducesResponseType(typeof(NewsModel), 201)]
        public ActionResult CreateNews(CreateNewsValidator newsValidator)
        {
            NewsModel news = (NewsModel)newsValidator;
            UsersModel user = _usersService.getUser(HttpContext.User);

            news.Author = $"{user.FirstName} {user.LastName}";

            _newsService.Create(news);

            return Ok(news);
        }

        /// <summary>
        /// Delete news
        /// </summary>
        /// <remarks>
        /// 
        ///     Delete /news/:id
        ///     
        /// </remarks>
        /// <param name="id">News's id</param>
        /// <returns></returns>
        [HttpDelete("{id:length(24)}")]
        [Authorize(Roles = "Admin")]
        [ProducesResponseType(204)]
        public ActionResult DeteleNews([FromRoute]string id)
        {
            _newsService.Remove(id);

            return NoContent();
        }

        /// <summary>
        /// Update news
        /// </summary>
        /// <remarks>
        ///     PATCH /news/:id
        /// </remarks>
        /// <param name="id">New's id</param>
        /// <param name="newsValidator"></param>
        /// <returns></returns>
        [HttpPatch("{id:length(24)}")]
        [Authorize(Roles = "Admin")]
        [ProducesResponseType(typeof(NewsModel), 201)]
        [ProducesResponseType(typeof(ErrorModel), 400)]
        public ActionResult UpdateNews([FromRoute]string id, [FromBody]UpdateNewsValidator newsValidator)
        {
            NewsModel oldNews = _newsService.Get(id);

            if (oldNews == null)
            {
                throw new CustomNotFound("News not found");
            }

            NewsModel newNews = (NewsModel)newsValidator;

            newNews.MergeWith<NewsModel>(oldNews);

            _newsService.Update(id, newNews);

            return Ok(newNews);
        }

        /// <summary>
        /// List all news
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [ProducesResponseType(typeof(List<NewsModel>), 200)]
        public ActionResult ListNews([FromQuery] int limit)
        {
            return Ok(_newsService.List(limit));
        }

    }
}