﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RestApi.Filters
{
    public class ValidationProblemDetailsResult : IActionResult
    {
        /// <summary>
        /// This method is called after the execution of the process (validation, controller)
        /// It check if context.ModelState contain, and format the output and rewrite an HttpResponse
        /// context.ModelState.Value.Errors is usualy set by validator
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task ExecuteResultAsync(ActionContext context)
        {
            var modelStateEntries = context.ModelState.Where(e => e.Value.Errors.Count > 0).ToArray();
            var errors = new List<String>();


            foreach (var modelStateEntry in modelStateEntries)
            {
                foreach (var modelStateError in modelStateEntry.Value.Errors)
                {
                    errors.Add(modelStateError.ErrorMessage);
                }
            }

            // format string as a json, but don"t convert it to object
            string text = "{\"errors\":[";

            foreach (String data in errors.ToArray())
            {
                text += $"\"{data}\"";
                text += ",";
            }

            text = text.Substring(0, text.Length - 1);

            text += "]}";

            // transform string to byte array
            byte[] byteArray = Encoding.UTF8.GetBytes(text);

            //  set StatusCode to 400 (bad request) to match validation error
            context.HttpContext.Response.StatusCode = 400;
            context.HttpContext.Response.ContentType = "application/json";
            // write byte array in Response.body byteArray is in json format
            context.HttpContext.Response.Body.Write(byteArray, 0, byteArray.Length);
            return Task.CompletedTask;
        }
    }
}
