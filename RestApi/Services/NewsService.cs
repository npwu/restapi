﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using RestApi.Models;

namespace RestApi.Services
{
    public class NewsService
    {
        private readonly IMongoCollection<NewsModel> _news;

        public NewsService(IOptions<MongoDbSettings> settings)
        {
            _news = settings.Value.mongoDatabase.GetCollection<NewsModel>("News");
        }

        public List<NewsModel> List(int limit)
        {
            limit = (limit > 5) ? 5 : limit;
            return _news.Find(bson => true).SortByDescending(bson => bson.Date).Limit(limit).ToList();
        }

        public NewsModel Get(string id)
        {
            var res = _news.Find(bson => bson.Id == id).FirstOrDefault();
            if (res == null)
            {
                throw new CustomNotFound("Can not find this news");
            }
            return res;
        }

        public NewsModel Create(NewsModel news)
        {
            _news.InsertOne(news);
            return news;
        }

        public void Update(string id, NewsModel news)
        {
            ReplaceOneResult res = _news.ReplaceOne(bson => bson.Id == id, news);
            if (!res.IsAcknowledged)
            {
                throw new CustomInternalError("Can not replace user");
            }
        }

        public void Remove(String id)
        {
            DeleteResult res = _news.DeleteOne(bson => bson.Id == id);
            if (!res.IsAcknowledged)
            {
                throw new CustomInternalError("Can not delete user");
            }
        }
    }
}
