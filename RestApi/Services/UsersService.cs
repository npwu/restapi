﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestApi.Models;
using MongoDB.Driver;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using BCrypt.Net;
using RestApi.Utils;
using Microsoft.Extensions.Options;

namespace RestApi.Services
{
    public class UsersService
    {
        private readonly IMongoCollection<UsersModel> _users;
        public UsersService(IOptions<MongoDbSettings> settings)
        {
            _users = settings.Value.mongoDatabase.GetCollection<UsersModel>("Users");
        }

        public List<UsersModel> List(int perPage, int page)
        {
            return _users.Find(bson => true).Skip(perPage * (page - 1)).Limit(perPage).ToList();
        }

        public UsersModel Get(string id)
        {
            return _users.Find<UsersModel>(bson => bson.Id == id).FirstOrDefault();
        }

        public UsersModel Get(string email, string password)
        {
            UsersModel user = _users.Find<UsersModel>(bson => bson.Email == email).FirstOrDefault();
            if (user != null && HashPassword.ValidatePassword(password, user.GetPassword()))
            {
                return user;
            }
            return null;
        }

        public UsersModel Create(UsersModel user)
        {
            user.SetPassword(HashPassword.Hash(user.GetPassword()));
            UsersModel currentUser = _users.Find<UsersModel>(bson => bson.Email == user.Email).FirstOrDefault();
            if (currentUser != null)
            {
                return null;
            }
            _users.InsertOne(user);
            return user;
        }

        public void Update(string id, UsersModel bookIn)
        {
            _users.ReplaceOne(bson => bson.Id == id, bookIn);
        }

        public void Remove(UsersModel bookIn)
        {
            _users.DeleteOne(bson => bson.Id == bookIn.Id);
        }
        public void Remove(string id)
        {
            _users.DeleteOne(bson => bson.Id == id);
        }

        public UsersModel getUser(ClaimsPrincipal user)
        {
            string userId = user.Claims.FirstOrDefault(bson => bson.Type == ClaimTypes.NameIdentifier).Value;
            return this.Get(userId);
        }

        public bool EmailExist(string email)
        {
            UsersModel currentUser = _users.Find<UsersModel>(bson => bson.Email == email).FirstOrDefault();
            if (currentUser != null)
            {
                return false;
            }
            return true;
        }
    }
}
