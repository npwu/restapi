﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using RestApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApi.Services
{
    public class MessageService
    {
        private readonly IMongoCollection<MessageModel> _message;
        public MessageService(IOptions<MongoDbSettings> settings)
        {
            _message = settings.Value.mongoDatabase.GetCollection<MessageModel>("Messages");
        }

        public List<MessageModel> GetNewMessages(string userId)
        {
            return _message.Find(bson => bson.ToUser == userId && bson.IsRead == false).ToList();
        }

        public List<MessageModel> getChat(string FromUser, string ToUser)
        {
            List<MessageModel> messages = _message.Find(bson => (bson.ToUser == ToUser && bson.FromUser == FromUser) || (bson.ToUser == FromUser && bson.FromUser == ToUser)).SortBy(bson => bson.Date).Limit(40).ToList();

            foreach (MessageModel tmp in messages)
            {
                var filter = Builders<MessageModel>.Filter.Where(bson => bson.Id == tmp.Id && bson.FromUser != FromUser);
                var update = Builders<MessageModel>.Update.Set(bson => bson.IsRead, true);
                var options = new FindOneAndUpdateOptions<MessageModel>();
                _message.FindOneAndUpdate(filter, update, options);
            }

            return messages;
        }

        public void AddMessage(MessageModel messageModel)
        {
            _message.InsertOne(messageModel);
        }
    }
}
