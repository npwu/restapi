﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestApi.Validator
{
    public class CreateNewsValidator
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
    }

    public class UpdateNewsValidator
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
