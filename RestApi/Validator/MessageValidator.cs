﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestApi.Validator
{
    public class MessageValidator
    {
        [Required]
        public string ToUser { get; set; }
        [Required]
        public string Content { get; set; }
    }
}
