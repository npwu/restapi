﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using RestApi.Models;


namespace RestApi.Middlewares
{
    /// <summary>
    /// This middleware is used to catch custom throw and generate an error message
    /// </summary>
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            // set default value
            var code = HttpStatusCode.InternalServerError;

            code = (ex is CustomNotFound) ? HttpStatusCode.NotFound : code;
            code = (ex is CustomInternalError) ? HttpStatusCode.InternalServerError : code;
            code = (ex is CustomBadRequest) ? HttpStatusCode.BadRequest : code;
            code = (ex is CustomUnauthorized) ? HttpStatusCode.Unauthorized : code;
            
            // set errors as string[] to match CustomValidationErrors patern
            var result = JsonConvert.SerializeObject(new { errors = new string[] { ex.Message } });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
}
