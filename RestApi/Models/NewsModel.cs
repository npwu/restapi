﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using RestApi.Validator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApi.Models
{
    public class NewsModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Title")]
        public string Title { get; set; }

        [BsonElement("Content")]
        public string Content { get; set; }

        [BsonElement("Date")]
        public DateTime Date { get; set; }

        [BsonElement("Author")]
        public String Author { get; set; }

        public static explicit operator NewsModel(CreateNewsValidator v)
        {
            return new NewsModel
            {
                Title = v.Title,
                Content = v.Content,
                Author = null,
                Date = DateTime.Now
            };
        }

        public static explicit operator NewsModel(UpdateNewsValidator v)
        {
            return new NewsModel
            {
                Title = v.Title ?? null,
                Content = v.Content ?? null
            };
        }
    }
}
