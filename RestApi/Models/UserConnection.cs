﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApi.Models
{
    public class UserConnection
    {
        public string UserId { set; get; }
        public string ConnectionID { set; get; }
    }
}
