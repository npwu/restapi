﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApi.Models
{
    public class ErrorModel
    {
        public string message { get; set; }
    }

    public class CustomBadRequest : Exception
    {
        public CustomBadRequest(string message) : base(message)
        {
        }
    }

    public class CustomInternalError : Exception
    {
        public CustomInternalError(string message) : base(message)
        {
        }
    }

    public class CustomUnauthorized : Exception
    {
        public CustomUnauthorized(string message) : base(message)
        {
        }
    }

    public class CustomNotFound : Exception
    {
        public CustomNotFound(string message) : base(message)
        {
        }
    }
}
