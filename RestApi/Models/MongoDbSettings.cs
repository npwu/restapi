﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApi.Models
{
    public class MongoDbSettings
    {
        public MongoClient mongoClient;
        public IMongoDatabase mongoDatabase;
    }
}
