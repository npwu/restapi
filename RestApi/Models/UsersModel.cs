﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;
using RestApi.Validator;

namespace RestApi.Models
{
    public class UsersModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("FirstName")]
        public string FirstName { get; set; }

        [BsonElement("LastName")]
        public string LastName { get; set; }

        [BsonElement("Role")]
        public string Role { get; set; }

        [EmailAddress]
        [BsonElement("Email")]
        public string Email { get; set; }

        [BsonElement("Password")]
        private string Password { get; set; }

        public string GetPassword()
        {
            return Password;
        }

        public void SetPassword(string newPassword)
        {
            Password = newPassword;
        }

        public static explicit operator UsersModel(RegisterValidator v)
        {
            return new UsersModel
            {
                Email = v.Email,
                Password = v.Password,
                FirstName = v.FirstName,
                LastName = v.LastName,
                Role = null,
            };
        }

        public static explicit operator UsersModel(UpdateValidator v)
        {
            return new UsersModel
            {
                Email = v.Email ?? null,
                FirstName = v.FirstName ?? null,
                LastName = v.LastName ?? null
            };
            throw new NotImplementedException();
        }
    }
}
