﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RestApi.Services;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;
using RestApi.Models;
using MongoDB.Driver;
using Swashbuckle.AspNetCore.Swagger;
using System.Reflection;
using System.IO;
using RestApi.Middlewares;
using RestApi.Filters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Cors.Infrastructure;
using MQTTnet.AspNetCore;
using Microsoft.AspNetCore.SignalR;
using RestApi.Hubs;

namespace RestApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            CurrentEnvironment = env;
        }

        public IConfiguration Configuration { get; }
        private IHostingEnvironment CurrentEnvironment { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["Jwt:Issuer"],
                    ValidAudience = Configuration["Jwt:Issuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                };

                options.Events = new JwtBearerEvents()
                {
                    OnChallenge = context =>
                    {
                        throw new CustomUnauthorized("Unauthorized");
                    }
                };
            });
            

            // Setup for mongodb
            services.AddScoped<UsersService>();
            services.AddScoped<NewsService>();
            services.AddScoped<MessageService>();

            services.Configure<MongoDbSettings>(options =>
            {
                var ConnectionString
                    = Configuration.GetConnectionString("MongoUrl");

                string database = null;
                if (CurrentEnvironment.IsProduction())
                {
                    database = Configuration.GetSection("MongoDbDatabase:ProdDatabase").Value;
                } else if (CurrentEnvironment.IsDevelopment())
                {
                    database = Configuration.GetSection("MongoDbDatabase:DevDatabase").Value;
                } else
                {
                    database = Configuration.GetSection("MongoDbDatabase:TestDatabase").Value;
                }

                options.mongoClient = new MongoClient(ConnectionString);
                options.mongoDatabase = options.mongoClient.GetDatabase(database);

            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "RestApi", Version = "v1" });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            //this adds a hosted mqtt server to the services
            services.AddHostedMqttServer(builder => builder.WithDefaultEndpointPort(1883));

            //this adds tcp server support based on System.Net.Socket
            services.AddMqttTcpServerAdapter();

            //this adds websocket support
            services.AddMqttWebSocketServerAdapter();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = ctx => new ValidationProblemDetailsResult();
            });

            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            Console.WriteLine(env);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "RestApi v1");
                c.RoutePrefix = "doc";
            });

            app.UseMiddleware(typeof(ErrorHandlingMiddleware));

            app.UseMqttEndpoint();

            app.UseSignalR(routes =>
            {
                routes.MapHub<ChatHub>("/chatHub/signalR");
            });

            app.UseMvc();

        }

    }
}
